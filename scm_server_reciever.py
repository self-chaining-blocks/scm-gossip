# Copyright 2016 Anselm Binninger, Thomas Maier, Ralph Schaumann
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import socket
import json
import sys
import scm.file_util as fu
import scm.scm_state_util as ss
from scm.Configuration import *

config = Configuration()
from gossip.util import packing, message

__author__ = "Anselm Binninger, Ralph Schaumann, Thomas Maier"


def store_as_file(data):
    if not (":::" in data):
        return
    try:
        data = data.split(":::")
        device = data[0]
        filename = data[1]
        message = data[2]
        msg_obj = json.loads(message)
        if "fake_sign" in message:
            if "preparer" in message:
                # "seems like this need signature"
                # " so putting in signature queue"
                fu.write_to_file("data/signqueue::" + filename, message)
                return

    except Exception as e:
        print("not a good message" + str(e))
        return
    print("going for validation")
    valid, msg_type = ss.validate_this_message(json.loads(message))
    if not valid:
        print("BBBBBBBBAD RREQUEST")
        return
    print("validation OK")
    filename = data[1]
    message = data[2]
    fu.write_to_file("data/incoming::" + filename, message)
    print("wrote file")


try:
    if True:
        sock = socket.socket()
        print(config.gossip_ip_address)
        sock.connect((config.gossip_ip_address, 7010))
        values = packing.pack_gossip_notify(700)
        packing.send_msg(sock, values["code"], values["data"])
        l_host, l_port = sock.getsockname()
        print(l_host, l_port)
        while True:
            values = packing.receive_msg(sock)
            print(l_host, l_port)
            print(values)
            message_object = message.GOSSIP_MESSAGE_TYPES.get(
                values["code"], message.MessageOther
            )
            print("testing")
            msg_obj = message_object(values["message"]).get_values()["message"]
            data = ""
            for i in msg_obj:
                data += chr(i)
            print(data)
            store_as_file(data)
            print("testing")
            # if 500 <= values['code'] < 520:
            #    print(message_object(values['message']))
            # else:
            #    print(values)
        sock.close()
except Exception as e:
    print("%s" % e)
