FROM ubuntu:latest
MAINTAINER Raju Mariappan "raju@mydomain.com"
RUN apt-get update -y && \
    apt-get install -y python3-pip python3-dev
COPY ./requirements.txt /requirements.txt
RUN pip3 install -r requirements.txt
WORKDIR /
RUN mkdir data
RUN mkdir app

RUN mkdir /app/conf
RUN mkdir /app/documentation
RUN mkdir /app/examples
RUN mkdir /app/gossip
RUN mkdir /app/tests
COPY * /app/
COPY documentation /app/documentation/
COPY examples /app/examples/
COPY gossip  /app/gossip/
COPY tests /app/tests/
COPY conf  /app/conf/

WORKDIR /app
ENTRYPOINT [ "python3" ]
CMD [ "main.py" ]
