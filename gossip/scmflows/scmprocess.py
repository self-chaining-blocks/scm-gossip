# Copyright 2016 Anselm Binninger, Thomas Maier, Ralph Schaumann
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
import multiprocessing
import socket
import time

from gossip.communication.client_receiver import GossipClientReceiver

__author__ = "Anselm Binninger, Thomas Maier, Ralph Schaumann"


class ScmServer(multiprocessing.Process):
    def __init__(self, controller_to_scm):
        """SCM Business logics are here.
        :param connection_pool: New connections will be added to the appropriate connection pool
        """
        multiprocessing.Process.__init__(self)
        self.controller_to_scm = controller_to_scm
        self.my_prime_number = 3

    def init(self):
        pass

    def run(self):
        """Typical run method for the sender process. It waits for new connections, refers to newly instantiated
        receiver instances, and finally starts the new receivers."""
        try:
            logging.info("started SCM Server - PID: %s" % (self.pid))
            while True:
                queue_item = self.controller_to_scm.get()
                logging.info("SCM Server got new message")
                print(queue_item["message"].get_values())
        except OSError as os_error:
            logging.error("crashed PID: %s %s" % (self.pid, os_error))

    def register_with_some_nodes(self):
        # find the endpoints of nodes with index-1 , mod(index) , mod(index+1)
        pass

    def spread_message(self, message):
        notification_msg = convert.from_announce_to_notification(msg_id, message)
        for receiver in self.api_registration_handler.get_registrations(
            message.data_type
        ):
            if receiver != senders_identifier:
                self.to_api_queue.put(
                    {
                        "type": QUEUE_ITEM_TYPE_SEND_MESSAGE,
                        "identifier": receiver,
                        "message": notification_msg,
                    }
                )
